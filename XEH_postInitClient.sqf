///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Initialize BFT 
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "\a3\ui_f\hpp\definedikcodes.inc"

[] spawn {
	waitUntil {sleep 0.1;!(IsNull (findDisplay 46))};

	// Check if BFT is enabled
	if (ReddBftEnable) then {

		// Init variables
		ReddBFTgotItem = false;
		ReddBFTdisplayOpen = false; 
		ReddBFTsupportDisplayOpen = false;
		ReddBFTSupportGroups = []; 
		ReddBftMarkers = [];
		ReddBFTmapScaleSat = 0.08;
		ReddBFTmapScaleToPo = 0.08;
		ReddBFTLeftRight = 0;
		ReddPosOffset = 0.773437;
		ReddNegOffset = -0.773437;;

		// Keybind to open/close standard F8
		["Redd BFT", "BFTopenDisplay", "Open BFT mini display", {
			call redd_fnc_BFTdisplay;
		}, {}, [DIK_F8, [false, false, false]], false] call CBA_fnc_addKeybind;

		// Keybind to zoom minimap in standard F9
		["Redd BFT", "BFTzommInDisplay", "Zoom in mini display", {
			_scaleSat = ctrlMapScale ((uiNamespace getVariable "redd_bft") displayCtrl 86503);
			_scaleToPo = ctrlMapScale ((uiNamespace getVariable "redd_bft") displayCtrl 86504);
			ReddBFTmapScaleSat = _scaleSat * 0.8;
			ReddBFTmapScaleToPo = _scaleToPo * 0.8;
			true;
		}, {}, [DIK_F9, [false, false, false]], true] call CBA_fnc_addKeybind;

		// Keybind to zoom minimap out standard F10
		["Redd BFT", "BFTZoomOutDisplay", "Zoom out mini display", {
			_scaleSat = ctrlMapScale ((uiNamespace getVariable "redd_bft") displayCtrl 86503);
			_scaleToPo = ctrlMapScale ((uiNamespace getVariable "redd_bft") displayCtrl 86504);
			ReddBFTmapScaleSat = _scaleSat * 1.2;
			ReddBFTmapScaleToPo = _scaleToPo * 1.2;
			true;
		}, {}, [DIK_F10, [false, false, false]], true] call CBA_fnc_addKeybind;

		// Keybind to toggle minimap texture F11
		["Redd BFT", "BFTToggleMapTexture", "Toggle mini display map texture", {
			if (ctrlShown ((uiNamespace getVariable "redd_bft") displayCtrl 86503)) then {
				(uiNamespace getVariable "redd_bft") displayCtrl 86503 ctrlShow false;
			} else {
				(uiNamespace getVariable "redd_bft") displayCtrl 86503 ctrlShow true;
			}
		}, {}, [DIK_F11, [false, false, false]], true] call CBA_fnc_addKeybind;

		// Keybind to switch mini display and support display left<->right F12
		["Redd BFT", "BFTToggleLeftRight", "Toggle mini display left-right", {
			if (ReddBFTLeftRight == 0) then {
				[ReddNegOffset] call redd_fnc_toggleLeftRightDisplay;
				[ReddNegOffset] call redd_fnc_toggleLeftRightSupport;
				ReddBFTLeftRight = 1;
			} else {
				[ReddPosOffset] call redd_fnc_toggleLeftRightDisplay;
				[ReddPosOffset] call redd_fnc_toggleLeftRightSupport;
				ReddBFTLeftRight = 0;
			};
			
		}, {}, [DIK_F12, [false, false, false]], true] call CBA_fnc_addKeybind;

		// Run the loops
		[{_this call redd_fnc_updateBFT}, 1,[]] call CBA_fnc_addPerFrameHandler;
		[{_this call redd_fnc_updateSupport}, 1,[]] call CBA_fnc_addPerFrameHandler;

		// Add groupname change button and unittype change combobox to map
		with uiNamespace do {
			ReddBFTgroupNameButton = (findDisplay 12) ctrlCreate ["RscButton",-1];
			ReddBFTgroupNameButton ctrlSetPosition [0.46849 * safezoneW + safezoneX, 0.00500001 * safezoneH + safezoneY, 0.0670312 * safezoneW, 0.022 * safezoneH];
			ReddBFTgroupNameButton ctrlSetText "Set group name:";
			ReddBFTgroupNameButton ctrlCommit 0;

			ReddBFTgroupNameEdit = (findDisplay 12) ctrlCreate ["RscEdit",-1];
			ReddBFTgroupNameEdit ctrlSetPosition [0.54125 * safezoneW + safezoneX, 0.00500001 * safezoneH + safezoneY, 0.0979687 * safezoneW, 0.022 * safezoneH];
			ReddBFTgroupNameEdit ctrlSetText format ["%1", player getVariable ["ReddBFTGroupName", ""]];
			ReddBFTgroupNameEdit ctrlCommit 0;

			ReddBFTgroupTypeText = (findDisplay 12) ctrlCreate ["RscText",-1];
			ReddBFTgroupTypeText ctrlSetPosition [0.325 * safezoneW + safezoneX, 0.00500001 * safezoneH + safezoneY, 0.0979687 * safezoneW, 0.022 * safezoneH];
			ReddBFTgroupTypeText ctrlSetText "Unit Type:";
			ReddBFTgroupTypeText ctrlCommit 0;

			ReddBFTgroupTypeEdit = (findDisplay 12) ctrlCreate ["RscCombo",-1];
			ReddBFTgroupTypeEdit ctrlSetPosition [0.37 * safezoneW + safezoneX, 0.00500001 * safezoneH + safezoneY, 0.0979687 * safezoneW, 0.022 * safezoneH];
			ReddBFTgroupTypeEdit ctrlCommit 0;
			{
				ReddBFTgroupTypeEdit lbAdd _x;
			} forEach [
				"Infantry",
				"Motorized Infantry",
				"Mechanized Infantry",
				"Armor",
				"Recon",
				"Helicopter",
				"Plane",
				"UAV",
				"Naval",
				"Medical",
				"Artillery",
				"HQ",
				"Support",
				"Maintenance"
			];
			{
				ReddBFTgroupTypeEdit lbSetData [_forEachIndex, _x];
			} forEach [
				"inf",
				"motor_inf",
				"mech_inf",
				"armor",
				"recon",
				"air",
				"plane",
				"uav",
				"naval",
				"med",
				"art",
				"hq",
				"support",
				"maint"
			];
			ReddBFTgroupTypeEdit lbSetCurSel ([
				"inf",
				"motor_inf",
				"mech_inf",
				"armor",
				"recon",
				"air",
				"plane",
				"uav",
				"naval",
				"med",
				"art",
				"hq",
				"support",
				"maint"
			] find (player getVariable ['ReddBftUnitType', 'inf']));
			ReddBFTgroupTypeEdit ctrlAddEventHandler[ "LBSelChanged", {player setVariable ['ReddBftUnitType',(_this select 0) lbData (lbCurSel (_this select 0)), true];}];
		};

		// Add groupname change button function
		ReddBFTgroupNameButton_function = {
			with uiNamespace do {
				player setVariable ["ReddBFTGroupName", ctrlText ReddBFTgroupNameEdit];
			};
		};

		// Add groupname change button action
		with uiNamespace do {
			ReddBFTgroupNameButton ctrlAddEventHandler ["ButtonClick", missionNamespace getVariable "ReddBFTgroupNameButton_function"];
			ReddBFTgroupNameButton ctrlCommit 0;
		};
	};
};