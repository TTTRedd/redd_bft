///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd, Katalam, Corben
//
//	Description: Drawns BFT markers
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_markerText", "_markerPosition", "_markerType", "_markerColor", "_unit"];

// Check if marker exists, this happens when unit leaves group we have a group name twice and need a new one for the new group
if !({_x isEqualTo _markerText} count ReddBftGroupNames > 1) then {
	private _marker = createMarkerLocal ["ReddBftMarker_" + _markerText, _markerPosition];
	_marker setMarkerTypeLocal _markerType; 
	_marker setMarkerColorLocal _markerColor;
	_marker setMarkerTextLocal _markerText;
	_marker;
};
