///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd, Katalam, Corben
//
//	Description: Drawns group internal BFT markers, e.q. fireteam leader markers
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_index", "_markerPosition", "_markerColor", "_markerText", "_unit"];

private _marker = createMarkerLocal ["ReddBftMarkerGroup_" + _markerText + "-" + str _index, _markerPosition];
_marker setMarkerTypeLocal "mil_box_noShadow";
_marker setMarkerDirLocal 45;
_marker setMarkerColorLocal _markerColor;
_marker setMarkerTextLocal str (_index + 1);
_marker;