///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd, Katalam
//
//	Description: Gets units from group with BFT item and selects the leader 
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

params ["_group"];

// Check if unit has BFT item and count them
private _units = (units _group) select {"redd_bft_inventoryItem" in items _x || "redd_bft_inventoryItem" in assignedItems _x};
private _count = count _units;
// If we found at least one unit with BFT item get the first unit in group (normaly the leader) and exit
if (_count >= 1) exitWith {
	_units select 0;
};
// If there is no unit with BFT item in return objNull
objNull;