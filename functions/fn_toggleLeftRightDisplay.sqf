///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Toggle mini display position left<->right (ReddBFTLeftRight = 0 = right, ReddBFTLeftRight = 1 = left) 
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

params ["_offset"];

// Move _bftDisplay depending on _offset
private _bftDisplay = uiNamespace getVariable "redd_bft";
for "_i" from 86501 to 86514 do { 
	_control = _bftDisplay displayCtrl _i; 
	_posX = (ctrlPosition _control) select 0;
	_factorXold = (_posX - safezoneX) / safezoneW;
	_factorXnew = _factorXold + _offset;
	_control ctrlSetPosition [_factorXnew * safezoneW + safezoneX, (ctrlPosition _control) select 1];  
	_control ctrlCommit 0; 
};