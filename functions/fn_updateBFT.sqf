///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd, Katalam, Corben
//
//	Description: Updates BFT marker positions
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

// Delete all markers so we can update them
{
    deleteMarkerLocal _x;
} forEach ReddBftMarkers;
// Initialize global arrays
ReddBftMarkers = [];
ReddBftGroupNames= [];
// initialize simple variables
private _leader = leader player;
private _leaderSide = side _leader;
private _leaderGroup = group _leader;
private _leaderPosition = getPos _leader;
private _ftl = [];
private _ftlOtherGroup = [];
private "_leaderGroupTypePrefix";
private "_leaderSideColor";
private "_leaderMarkerType";
_leaderGroup deleteGroupWhenEmpty true;
// Only go on if player has BFT item
if !("redd_bft_inventoryItem" in items player || "redd_bft_inventoryItem" in assignedItems player) exitWith { 
    // Check CBA Setting
    if (ReddBFThideMarker) then {
        // If CBA setting = true hide all foreign markers for units without BFT item
        ReddBFTgotItem = false;
        {  
            //Get _id from Server
            private _idFromServer = missionNamespace getVariable [getPlayerUID player, "-"];
            // Split marker string to get something like [_USER_DEFINED, idstr, markerId, channelId] for user markers
            _markerSplitString = _x splitString " #/";
            // Get "_USER_DEFINED" string and user id as string from marker. Returns "_USER_DEFINED" from marker
            _userDefinedStringFromMarker =_markerSplitString select 0;
            _userIdStringFromMarker =_markerSplitString select 1;
            // Convert id as string from marker to number
            _userIdStringFromMarkerAsNumber = parseNumber _userIdStringFromMarker;
            // Check if marker is a user marker
            if (_userDefinedStringFromMarker isEqualTo "_USER_DEFINED") then {
                if !(isNil "_userIdStringFromMarkerAsNumber" || isNil "_idFromServer") then {
                    // Compare id from server with id as number from marker to not delete own user markers
                    if !(_userIdStringFromMarkerAsNumber == _idFromServer) then {
                        _x setMarkerAlphaLocal 0;
                    }; 
                };
            };
        } forEach allMapMarkers;
    };
};
// Check CBA Setting
if (ReddBFThideMarker) then {
    // If CBA setting = true unhide all markers for units with BFT item
    if !(ReddBFTgotItem) then {
        {   
            // Unhide all markers if unit has BFT item
            _x setMarkerAlphaLocal 1;
        } forEach allMapMarkers;
        ReddBFTgotItem = true;
    };
};
// Get Marker icon, or set to default "inf" if it is not set
private _leaderGroupTypeSuffix = _leader getVariable ["ReddBftUnitType", "inf"];
if (_leaderGroupTypeSuffix isEqualTo "") then {
    _leaderGroupTypeSuffix = "inf";
};
// Set unit type for every unit in group 
{
    _x setVariable ["ReddBftUnitType", _leaderGroupTypeSuffix, true];
} forEach units _leaderGroup;
// Get BFT marker name, set to vanilla groupid if it is not set or set vanilla groupid to bft marker name if it is set
private _leaderGroupName = _leader getVariable ["ReddBFTGroupName", ""];
if (_leaderGroupName isEqualTo "") then {
    _leaderGroupName = groupId _leaderGroup;
} else {
    _leaderGroup setGroupId [_leaderGroupName];
};
// Save group name variable to every unit in group 
private _countSubGroups = 0;
{   
    // Set subgroupname for groupmember with bft item, so they will create a new group with old name + subgroup suffix when leaving the initial group
    if (("redd_bft_inventoryItem" in items _x || "redd_bft_inventoryItem" in assignedItems _x) && _x != _leader) then {
        // Only set name one time
        if !(_x getVariable ["hasBeenCounted", false]) then {
            _x setVariable ["hasBeenCounted", true, true];
            _countSubGroups = _countSubGroups + 1;
            _x setVariable ["ReddBftGroupName", _leaderGroupName + "-" + str _countSubGroups, true];
        };
        _ftl pushBack _x;
    } else {
        // Save group name variable to every unit in group without bft
        _x setVariable ["ReddBftGroupName", _leaderGroupName, true];
    }; 
} forEach units _leaderGroup;
// Save in global variable
ReddBftGroupNames pushBack _leaderGroupName;
// switch case for player side
switch (_leaderSide) do {
    case west: {
        _leaderGroupTypePrefix = "b";
        _leaderSideColor = "colorBLUFOR";
    };
    case east: {
        _leaderGroupTypePrefix = "o";
        _leaderSideColor = "ColorEAST";
    };
    case resistance: {
        _leaderGroupTypePrefix = "n";
        _leaderSideColor = "ColorGUER";
    };
};
// Build marker type from prefix (side) and suffix (type) if markertype is set to "static"
if (ReddStaticUnitType) then {
    _leaderMarkerType = _leaderGroupTypePrefix + "_" + _leaderGroupTypeSuffix;
} else {
    //Using ACE function to get dynamic markers if markertype is set to "dynamically"
    _leaderMarkerType = [_leaderGroup] call ace_common_fnc_getmarkerType;
};
// call function to draw own group marker
private _leaderMarker = [_leaderGroupName, _leaderPosition, _leaderMarkerType, _leaderSideColor, _leader] call redd_fnc_drawMarker;
// Save in global variable
ReddBftMarkers pushBack _leaderMarker;
// Get team color and position from every ftl in own group and call function to draw internal group markers
{
    private _teamColor = 0;
    _ftlPos = getPos _x;
    // Switch through team colors
    switch (assignedTeam _x) do {
        case "RED": {
            _teamColor = "ColorRed";
        };
        case "GREEN": {
            _teamColor = "ColorGreen";
        };
        case "BLUE": {
            _teamColor = "ColorBlue";
        };
        case "YELLOW": {
            _teamColor = "ColorYellow";
        };
        default {
            _teamColor = ["ColorUNKNOWN", "colorIndependent", "colorBLUFOR", "colorOPFOR"] select ((["UNKNOWN", "GUER", "WEST", "EAST"] find str _leaderSide) max 0);
        };
    };
    // Get position from every ftl in group and call function to draw internal markers
    private _unitPosition = getPos _x;
    ReddBftMarkers pushBack ([_forEachIndex, _unitPosition, _teamColor, _leaderGroupName, _x] call redd_fnc_drawInternalMarker); 
} forEach _ftl;
// Create markers for other groups
// Get all other groups from same side
private _allGroupsPlayerSide = allGroups select {side _x isEqualTo _leaderSide && {_x != _leaderGroup}};
{
    // Save the group in a more readable variable;
    private _otherGroup = _x;
    // Set simple variables
    private "_otherGroupTypeSuffix";
    private "_otherGroupMarkerType";
    _otherGroup deleteGroupWhenEmpty true;
    private _leaderOtherGroup = leader _otherGroup;
    // Get Marker icon, or set to default "inf" if it is not set
    private _otherGroupTypeSuffix = _leaderOtherGroup getVariable ["ReddBftUnitType", "inf"];
    if (_otherGroupTypeSuffix isEqualTo "") then {
        _otherGroupTypeSuffix = "inf";
    };
    // Build marker type from prefix (side) and suffix (type) if markertype is set to "static"
    if (ReddStaticUnitType) then {
        _otherGroupMarkerType = _leaderGroupTypePrefix + "_" + _otherGroupTypeSuffix;
    } else {
        //Using ACE function to get dynamic markers if markertype is set to "dynamically"
        _otherGroupMarkerType = [_leaderOtherGroup] call ace_common_fnc_getmarkerType;
    };
    // Get BFT marker name, set to vanilla groupid if it is not set or set vanilla groupid to bft marker name if it is set
    private _leaderOtherGroupName = _leaderOtherGroup getVariable ["ReddBftGroupName", ""];
    if (_leaderOtherGroupName isEqualTo "") then {
        _leaderOtherGroupName = groupId _otherGroup;
    } else {
        _otherGroup setGroupId [_leaderOtherGroupName];
    };
    // Save in global variable
    ReddBftGroupNames pushBack _leaderOtherGroupName;
    // Find units in all other groups with bft item, so only they will get a marker
    private _unitWithItem = [_leaderOtherGroup] call redd_fnc_getBftItem;
    // Get the position for the first unit with BFT item from group (normaly the leader)  
    private _leaderOtherGroupPosition = getPos _unitWithItem;
    // If no unit in group has a BFT item we returned objNull, position for objNull is always [0,0,0] and we dont want a marker and exit  
    if (_leaderOtherGroupPosition isEqualTo [0, 0, 0]) exitWith {};
    // call function to draw other group marker
    private _marker = [_leaderOtherGroupName, _leaderOtherGroupPosition, _otherGroupMarkerType, _leaderSideColor, _leaderOtherGroup] call redd_fnc_drawMarker;
    // Save marker in global array
    ReddBftMarkers pushBack _marker;
} forEach _allGroupsPlayerSide;