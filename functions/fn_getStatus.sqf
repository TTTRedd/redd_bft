///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Get BFT support groups names, status colors and status code
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

{
	// Get Variables
	private "_statusText";
	private "_statusColor";
	private _group = _x;
	private _leader = leader _group;
	private _groupIndex = ReddBFTSupportGroups find _group;
	private _groupCount = count ReddBFTSupportGroups;

	// Get BFT marker name, set to vanilla groupid if it is not set or set vanilla groupid to bft marker name if it is set
	private _leaderGroupName = _leader getVariable ["ReddBftGroupName", ""]; 
    if (_leaderGroupName isEqualTo "") then {
        _leaderGroupName = groupId _group;
    } else {
        _group setGroupId [_leaderGroupName];
    };
	private _groupStatus = _leader getVariable ["ReddBFTstatus", ""];
	private _defaultColor = [0, 0, 0, 0.5];

	// Set group displays to default values. Need this if there was a group in the display which has been disbanded
	for "_i" from (_groupCount + 86516) to 86521 do {
		((uiNamespace getVariable "redd_bft_support") displayCtrl _i) ctrlSetBackgroundColor _defaultColor;
	};
	for "_i" from (_groupCount + 86522) to 86527 do {
		((uiNamespace getVariable "redd_bft_support") displayCtrl _i) ctrlSetText "-";
	};
	for "_i" from (_groupCount + 86528) to 86533 do {
		((uiNamespace getVariable "redd_bft_support") displayCtrl _i) ctrlSetText "-";
	};

	// Only for the first 6 groups because we only have 6 fields
	if (_groupIndex == 0 || _groupIndex < 6) then {

		// Get status color
		switch (_groupStatus) do {
			case "1": {_statusColor = [0.133,1,0.133,0.5]}; //Dark Green Rdy in base
			case "2": {_statusColor = [0.45,1,0.133,0.5]}; //Light Green Rdy on radio
			case "3": {_statusColor = [1,1,0.133,0.5]}; //Yellow Prep assignment
			case "4": {_statusColor = [1,0.5,0.233,0.5]}; //Light Orange Moving to SP
			case "5": {_statusColor = [0.133,0.445,1,0.5]}; //Blue Reached SP
			case "6": {_statusColor = [1,0.375,0.1,0.5]}; //Dark Orange Leaving supply point
			case "7": {_statusColor = [1,0.133,0.133,0.5]}; //Red Not Rdy
		};

		// Set status color
		switch (_groupIndex) do {
			case 0: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86516) ctrlSetBackgroundColor _statusColor;};
			case 1: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86517) ctrlSetBackgroundColor _statusColor;};
			case 2: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86518) ctrlSetBackgroundColor _statusColor;};
			case 3: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86519) ctrlSetBackgroundColor _statusColor;};
			case 4: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86520) ctrlSetBackgroundColor _statusColor;};
			case 5: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86521) ctrlSetBackgroundColor _statusColor;};
		};

		// Set group names
		switch (_groupIndex) do {
			case 0: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86522) ctrlSetText format ["%1", _leaderGroupName];};
			case 1: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86523) ctrlSetText format ["%1", _leaderGroupName];};
			case 2: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86524) ctrlSetText format ["%1", _leaderGroupName];};
			case 3: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86525) ctrlSetText format ["%1", _leaderGroupName];};
			case 4: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86526) ctrlSetText format ["%1", _leaderGroupName];};
			case 5: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86527) ctrlSetText format ["%1", _leaderGroupName];};
		};

		// Get status Text
		switch (_groupStatus) do {
			case "1": {_statusText = "Rdy in base"};
			case "2": {_statusText = "Rdy on radio"};
			case "3": {_statusText = "Prep assignment"};
			case "4": {_statusText = "Moving to SP"};
			case "5": {_statusText = "Reached SP"};
			case "6": {_statusText = "Leaving SP"};
			case "7": {_statusText = "Not Rdy"};
		};

		// Set status text
		switch (_groupIndex) do {
			case 0: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86528) ctrlSetText _statusText;};
			case 1: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86529) ctrlSetText _statusText;};
			case 2: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86530) ctrlSetText _statusText;};
			case 3: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86531) ctrlSetText _statusText;};
			case 4: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86532) ctrlSetText _statusText;};
			case 5: {((uiNamespace getVariable "redd_bft_support") displayCtrl 86533) ctrlSetText _statusText;};
		};
	};
} forEach ReddBFTSupportGroups;