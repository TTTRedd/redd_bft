///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Show BFT data
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

// Animate map
((uiNamespace getVariable "redd_bft") displayCtrl 86503) ctrlMapAnimAdd [0, ReddBFTmapScaleSat, getPosASL player];
((uiNamespace getVariable "redd_bft") displayCtrl 86504) ctrlMapAnimAdd [0, ReddBFTmapScaleToPo, getPosASL player];
ctrlMapAnimCommit ((uiNamespace getVariable "redd_bft") displayCtrl 86503);
ctrlMapAnimCommit ((uiNamespace getVariable "redd_bft") displayCtrl 86504);
// Get BFT display data
private _gridPos = mapGridPosition player;
private _direction = direction player;
private _height = (getPosASL player) select 2;
((uiNamespace getVariable "redd_bft") displayCtrl 86505) ctrlSetText format ["Pos: %1", _gridPos];
((uiNamespace getVariable "redd_bft") displayCtrl 86506) ctrlSetText format ["Dir: %1",round _direction];
((uiNamespace getVariable "redd_bft") displayCtrl 86507) ctrlSetText format ["Alt: %1",round _height];
// Only show distance while on map
if !(visibleMap) then {
	((uiNamespace getVariable "redd_bft") displayCtrl 86508) ctrlSetText format ["Dist: -"];
	((uiNamespace getVariable "redd_bft") displayCtrl 86509) ctrlSetText format ["Rel.Dir: -"];
} else {
	private _mouseWorldCoord = (findDisplay 12 displayCtrl 51) ctrlMapScreenToWorld getMousePosition;
	private _distanceMousePlayer = _mouseWorldCoord distance2D getPos player;
	private _dirRel = player getDir _mouseWorldCoord;
	((uiNamespace getVariable "redd_bft") displayCtrl 86508) ctrlSetText format ["Dist: %1",round _distanceMousePlayer];
	((uiNamespace getVariable "redd_bft") displayCtrl 86509) ctrlSetText format ["Rel.Dir: %1",round _dirRel];
};
// Show own status only for support groups
if (leader player getVariable ['IsReddBFTsupport', false]) then {
	private _status = leader player getVariable ['ReddBFTstatus', '-'];
	((uiNamespace getVariable "redd_bft") displayCtrl 86510) ctrlSetText format ["Status: %1", _status];
} else {
	((uiNamespace getVariable "redd_bft") displayCtrl 86510) ctrlSetText format ["Status: -"];
};