///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Get all support groups to update them
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

if ("redd_bft_inventoryItem" in items player || "redd_bft_inventoryItem" in assignedItems player) then {
	ReddBFTSupportGroups = []; 
	//Get all groups from player side
	private _allGroupsPlayerSide = allGroups select {side _x isEqualTo side player};
	{
		// Get group and leader
		_group = _x;
		_leaderGroup = leader _group;
		// Check if group is support group and save support group in global array
		if (_leaderGroup getVariable ['IsReddBFTsupport', false]) then {
			// Set variable for all group member
			{
				_x setVariable ['IsReddBFTsupport', true, true];
			} forEach units _group;
			// Save the group
			ReddBFTSupportGroups pushBack _group;
		};
	} forEach _allGroupsPlayerSide;
};