///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Open/close BFT mini display
//
///////////////////////////////////////////////////////////////////////////////////////////////////

if ("redd_bft_inventoryItem" in items player || "redd_bft_inventoryItem" in assignedItems player) then {
	if (!ReddBFTdisplayOpen) then {
		
		// Add BFT display
		"redd_bft_layerName" cutRsc ["redd_bft","PLAIN",0,true];

		[{
			// Get BFT data
			call redd_fnc_getBFTdata;

			// Check if BFT display has been closed or has been lost and remove perFrameEH
			if (!ReddBFTdisplayOpen || !("redd_bft_inventoryItem" in items player || "redd_bft_inventoryItem" in assignedItems player)) then {
				// Close BFT display
				ReddBFTdisplayOpen = false;
				ReddBFTsupportDisplayOpen = false;
				"redd_bft_layerName" cutText ["", "PLAIN"];
				[_this select 1] call CBA_fnc_removePerFrameHandler;
			};
		}, 0.05,[]] call CBA_fnc_addPerFrameHandler;

		if (ReddBFTLeftRight == 1) then {
			[ReddNegOffset] call redd_fnc_toggleLeftRightDisplay;
		};

		ReddBFTdisplayOpen = true;
	
	} else {
		if (!ReddBFTsupportDisplayOpen) then {
			
			// Add BFT display
			"redd_support_layerName" cutRsc ["redd_bft_support","PLAIN",0,true];

			[{
				// Get support group status
				call redd_fnc_getStatus;

				// Check if BFT display has been closed or has been lost and remove perFrameEH
				if (!ReddBFTsupportDisplayOpen || !("redd_bft_inventoryItem" in items player || "redd_bft_inventoryItem" in assignedItems player)) then {
					// Close BFT display
					"redd_support_layerName" cutText ["", "PLAIN"];
					ReddBFTdisplayOpen = false;
					ReddBFTsupportDisplayOpen = false;
					[_this select 1] call CBA_fnc_removePerFrameHandler;
				};
			}, 0.05,[]] call CBA_fnc_addPerFrameHandler;

			if (ReddBFTLeftRight == 1) then {
				[ReddNegOffset] call redd_fnc_toggleLeftRightSupport;
			};

			ReddBFTsupportDisplayOpen = true;

		} else {
			// Close BFT display and support overlay
			ReddBFTdisplayOpen = false;
			ReddBFTsupportDisplayOpen = false;
			"redd_bft" cutText ["", "PLAIN"];
			"redd_support_layerName" cutText ["", "PLAIN"];
		};
	};
};