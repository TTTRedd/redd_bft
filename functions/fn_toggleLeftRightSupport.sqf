///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Toggle support display position left<->right (ReddBFTLeftRight = 0 = right, ReddBFTLeftRight = 1 = left) 
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

params ["_offset"];

// Move _supportDisplay depending on _offset
private _supportDisplay = uiNamespace getVariable "redd_bft_support";
for "_i" from 86516 to 86539 do {
	_control = _supportDisplay  displayCtrl _i; 
	_posX = (ctrlPosition _control) select 0;
	_factorXold = (_posX - safezoneX) / safezoneW;
	_factorXnew = _factorXold + _offset;
	_control ctrlSetPosition [_factorXnew * safezoneW + safezoneX, (ctrlPosition _control) select 1];  
	_control ctrlCommit 0;  
};