///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Add EH to transfer _id to client
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

// Check if isMultiplayer to get rid of error message in editor
if (isMultiplayer) then {
	addMissionEventHandler ["PlayerConnected", {
		params ["_id", "_uid", "_name", "_jip", "_idstr"];
		// Exit when dedi server is connecting
		if (_name isEqualTo "__SERVER__") exitWith {};
		// Save id
		missionNamespace setVariable [_uid, _id, true];
	}];
} else {
	// Save 0 as id
	missionNamespace setVariable ["_SP_PLAYER_", 0, true];
};