///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Define bft display and support display
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

class RscTitles {

	titles[] = {redd_bft, redd_bft_support};

	class redd_bft {
		idd = 86500;
		duration = 10e10;
		fadeIn = 0;
		fadout = 0;
		enableSimulation = 1;
		enableDisplay = 1;
		name = "redd_bft";
		onload = "uiNamespace setVariable ['redd_bft', _this select 0]";
		onUnLoad = "uiNamespace setVariable ['redd_bft', nil]";
		controlsBackground[] = {MapBackGround,MapToPo,MapSat};
		controls[] = {
			BackTop,
			BackBottom,
			FrameTop,
			FrameBottom,
			Frame,
			Position,
			Dir,
			Height,
			Distance,
			RelDir,
			Status
		};
		
		class Frame: RscPicture {
			idc = 86501;
			text = "redd_bft\pictures\frame_ca.paa";
			colorBackground[] = {1,1,1,0};
			colorText[] = {1,1,1,1};
			x = 0.799062 * safezoneW + safezoneX;
			y = 0.643 * safezoneH + safezoneY;
			w = 0.180469 * safezoneW;
			h = 0.286 * safezoneH;
		};

		class MapBackGround: RscPicture {
			idc = 86502;
			text = "#(argb,8,8,3)color(0.956,0.956,0.956,1)";
			colorBackground[] = {1,1,1,0};
			colorText[] = {1,1,1,1};
			x = 0.805 * safezoneW + safezoneX;
			y = 0.65 * safezoneH + safezoneY;
			w = 0.1675 * safezoneW;
			h = 0.27 * safezoneH;
		};

		class MapSat: RscMapControl {
			idc = 86503;
			colorBackground[] = {1,1,1,0};
			colorText[] = {1,1,1,0};
			scaleDefault = 0.08;
			maxSatelliteAlpha = 1;
			x = 0.805 * safezoneW + safezoneX;
			y = 0.65 * safezoneH + safezoneY;
			w = 0.170 * safezoneW;
			h = 0.27 * safezoneH;
		};

		class MapToPo: RscMapControl {
			idc = 86504;
			colorBackground[] = {1,1,1,0};
			colorText[] = {1,1,1,0};
			scaleDefault = 0.08;
			maxSatelliteAlpha = 0;
			x = 0.805 * safezoneW + safezoneX;
			y = 0.65 * safezoneH + safezoneY;
			w = 0.170 * safezoneW;
			h = 0.27 * safezoneH;
		};

		class Position: RscText {
			idc = 86505;
			shadow = 1;
			colorText[] = {0.75,0.75,0.75,1};
			x = 0.81 * safezoneW + safezoneX;
			y = 0.66 * safezoneH + safezoneY;
			w = 0.0876563 * safezoneW;
			h = 0.012 * safezoneH;
		};

		class Dir: RscText {
			idc = 86506;
			shadow = 1;
			colorText[] = {0.75,0.75,0.75,1};
			x = 0.869 * safezoneW + safezoneX;
			y = 0.66 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.012 * safezoneH;
		};

		class Height: RscText {
			idc = 86507;
			shadow = 1;
			colorText[] = {0.75,0.75,0.75,1};
			x = 0.919 * safezoneW + safezoneX;
			y = 0.66 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.012 * safezoneH;
		};

		class Distance: RscText {
			idc = 86508;
			shadow = 1;
			colorText[] = {0.75,0.75,0.75,1};
			x = 0.81 * safezoneW + safezoneX;
			y = 0.88 * safezoneH + safezoneY;
			w = 0.0825 * safezoneW;
			h = 0.012 * safezoneH;
		};

		class RelDir: RscText {
			idc = 86509;
			shadow = 1;
			colorText[] = {0.75,0.75,0.75,1};
			x = 0.865 * safezoneW + safezoneX;
			y = 0.88 * safezoneH + safezoneY;
			w = 0.064 * safezoneW;
			h = 0.012 * safezoneH;
		};

		class Status: RscText {
			idc = 86510;
			shadow = 1;
			colorText[] = {0.75,0.75,0.75,1};
			x = 0.93 * safezoneW + safezoneX;
			y = 0.88 * safezoneH + safezoneY;
			w = 0.0825 * safezoneW;
			h = 0.012 * safezoneH;
		};
		
		class BackTop: RscPicture {
			idc = 86511;
			colorBackground[] = {1,1,1,0};
			colorText[] = {0,0,0,0.75};
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.804219 * safezoneW + safezoneX;
			y = 0.654 * safezoneH + safezoneY;
			w = 0.170156 * safezoneW;
			h = 0.022 * safezoneH;
		};

		class BackBottom: RscPicture {
			idc = 86512;
			colorBackground[] = {1,1,1,0};
			colorText[] = {0,0,0,0.75};
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.804219 * safezoneW + safezoneX;
			y = 0.878 * safezoneH + safezoneY;
			w = 0.170156 * safezoneW;
			h = 0.022 * safezoneH;
		};

		class FrameTop: RscFrame {
			idc = 86513;
			x = 0.804714 * safezoneW + safezoneX;
			y = 0.65422 * safezoneH + safezoneY;
			w = 0.168698 * safezoneW;
			h = 0.0219999 * safezoneH;
			colorText[] = {0,0,0,1};
		};

		class FrameBottom: RscFrame {
			idc = 86514;
			x = 0.805886 * safezoneW + safezoneX;
			y = 0.877704 * safezoneH + safezoneY;
			w = 0.170156 * safezoneW;
			h = 0.022 * safezoneH;
			colorText[] = {0,0,0,1};
		};
	};

	class redd_bft_support {
		idd = 86515;
		duration = 10e10;
		fadeIn = 0;
		fadout = 0;
		enableSimulation = 1;
		enableDisplay = 1;
		name = "redd_bft_support";
		onload = "uiNamespace setVariable ['redd_bft_support', _this select 0]";
		onUnLoad = "uiNamespace setVariable ['redd_bft_support', nil]";
		controls[] = {
			Frame_Grp1,
			Frame_Grp2,
			Frame_Grp3,
			Frame_Grp4,
			Frame_Grp5,
			Frame_Grp6,
			Grp1,
			Grp2,
			Grp3,
			Grp4,
			Grp5,
			Grp6,
			Grp1_Name,
			Grp2_Name,
			Grp3_Name,
			Grp4_Name,
			Grp5_Name,
			Grp6_Name,
			Grp1_Status,
			Grp2_Status,
			Grp3_Status,
			Grp4_Status,
			Grp5_Status,
			Grp6_Status
		};

		class Grp1: RscText {
			idc = 86516;
			colorBackground[] = {0, 0, 0, 0.5};
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
		};

		class Grp2: RscText {
			idc = 86517;
			colorBackground[] = {0, 0, 0, 0.5};
			x = 0.897031 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
		};

		class Grp3: RscText {
			idc = 86518;
			colorBackground[] = {0, 0, 0, 0.5};
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.753 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
		};

		class Grp4: RscText {
			idc = 86519;
			colorBackground[] = {0, 0, 0, 0.5};
			x = 0.897031 * safezoneW + safezoneX;
			y = 0.753 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
		}; 

		class Grp5: RscText {
			idc = 86520;
			colorBackground[] = {0, 0, 0, 0.5};
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
		};

		class Grp6: RscText {
			idc = 86521;
			colorBackground[] = {0, 0, 0, 0.5};
			x = 0.897031 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
		};

		class Grp1_Name: RscText {
			idc = 86522;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
			x = 0.816511 * safezoneW + safezoneX;
			y = 0.689667 * safezoneH + safezoneY;
			w = 0.063073 * safezoneW;
			h = 0.0156297 * safezoneH;
		};

		class Grp2_Name: RscText {
            idc = 86523;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.899114 * safezoneW + safezoneX;
            y = 0.690704 * safezoneH + safezoneY;
            w = 0.063073 * safezoneW;
            h = 0.0156297 * safezoneH;
        };

		class Grp3_Name: RscText {
            idc = 86524;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.816667 * safezoneW + safezoneX;
            y = 0.756482 * safezoneH + safezoneY;
            w = 0.063073 * safezoneW;
            h = 0.0156297 * safezoneH;
        };

		class Grp4_Name: RscText {
            idc = 86525;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.898959 * safezoneW + safezoneX;
            y = 0.756482 * safezoneH + safezoneY;
            w = 0.063073 * safezoneW;
            h = 0.0156297 * safezoneH;
        };

		class Grp5_Name: RscText {
            idc = 86526;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.816666 * safezoneW + safezoneX;
            y = 0.823148 * safezoneH + safezoneY;
            w = 0.063073 * safezoneW;
            h = 0.0156297 * safezoneH;
        };

		class Grp6_Name: RscText {
            idc = 86527;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.898959 * safezoneW + safezoneX;
            y = 0.823149 * safezoneH + safezoneY;
            w = 0.063073 * safezoneW;
            h = 0.0156297 * safezoneH;
        };

		class Grp1_Status: RscText {
            idc = 86528;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.816491 * safezoneW + safezoneX;
            y = 0.71076 * safezoneH + safezoneY;
            w = 0.0629167 * safezoneW;
            h = 0.0156296 * safezoneH;
        };

		class Grp2_Status: RscText {
            idc = 86529;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.898958 * safezoneW + safezoneX;
            y = 0.711112 * safezoneH + safezoneY;
            w = 0.0629167 * safezoneW;
            h = 0.0156296 * safezoneH;
        };

		class Grp3_Status: RscText {
            idc = 86530;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.816667 * safezoneW + safezoneX;
            y = 0.777778 * safezoneH + safezoneY;
            w = 0.0629167 * safezoneW;
            h = 0.0156296 * safezoneH;
        };

		class Grp4_Status: RscText {
            idc = 86531;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.898958 * safezoneW + safezoneX;
            y = 0.778703 * safezoneH + safezoneY;
            w = 0.0629167 * safezoneW;
            h = 0.0156296 * safezoneH;
        };

		class Grp5_Status: RscText {
            idc = 86532;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.816667 * safezoneW + safezoneX;
            y = 0.843518 * safezoneH + safezoneY;
            w = 0.0629167 * safezoneW;
            h = 0.0156296 * safezoneH;
        };

		class Grp6_Status: RscText {
            idc = 86533;
			shadow = 1;
			text = "-";
			colorText[] = {0.75,0.75,0.75,1};
            x = 0.898958 * safezoneW + safezoneX;
            y = 0.843519 * safezoneH + safezoneY;
            w = 0.0629167 * safezoneW;
            h = 0.0156296 * safezoneH;
        };

		class Frame_Grp1: RscFrame {
			idc = 86534;
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
			colorText[] = {0,0,0,1};
		};

		class Frame_Grp2: RscFrame {
			idc = 86535;
			x = 0.897031 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
			colorText[] = {0,0,0,1};
		};

		class Frame_Grp3: RscFrame {
			idc = 86536;
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.753 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
			colorText[] = {0,0,0,1};
		};

		class Frame_Grp4: RscFrame {
			idc = 86537;
			x = 0.897031 * safezoneW + safezoneX;
			y = 0.753 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
			colorText[] = {0,0,0,1};
		};

		class Frame_Grp5: RscFrame {
			idc = 86538;
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
			colorText[] = {0,0,0,1};
		};

		class Frame_Grp6: RscFrame {
			idc = 86539;
			x = 0.897031 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.044 * safezoneH;
			colorText[] = {0,0,0,1};
		};
	};	
};	