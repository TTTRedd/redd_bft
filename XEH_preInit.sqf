///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: CBA settings
//			 
///////////////////////////////////////////////////////////////////////////////////////////////////

// Option to enable/disable BFT, default is disabled
[
	"ReddBftEnable",
	"CHECKBOX",
	["Enable BFT","Enable/Disable BFT"],
	"Redd BFT",
	false,
	1,
	{},
	true
] call CBA_fnc_addSetting;

// Option to choose if foreign markers will stay for units without bft item or not, default hide markers
[
	"ReddBFThideMarker",
	"CheckBox",
	["Hide foreign map markers for units without bft item","If checked, all foreign markers (Not markers set in Eden e.q. Objectives etc.) will be hidden for units without bft item. If not all markers will stay"],
	"Redd BFT",
	true,
	1,
	{},
	true
] call CBA_fnc_addSetting;

// Option to choose if marker will be shown static or dynamically, default is static
[
	"ReddStaticUnitType",
	"LIST",
	["Own marker unit type","'Static' will always show own NATO marker as predefined in Editor. 'Dynamically' will show NATO leader marker depending on in which vehicle the group leader is, outside of a vehicles the NATO leader marker will always be an infantry marker. Has no effect if shape of own marker is 'Diamond'"],
	"Redd BFT",
	[[true,false],["Static","Dynamically"],0],
	1,
	{},
	true
] call CBA_fnc_addSetting;