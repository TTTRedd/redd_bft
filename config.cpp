

class CfgPatches{
	class redd_bft{
        units[] = {
			"redd_bft_inventoryItem",
            "redd_bft_groundItem"
		};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
    };
};

#include "defines.hpp"
#include "display.hpp"

class CfgWeapons {

    class ItemGPS;

    class redd_bft_inventoryItem: ItemGPS {
        author = "redd";
        displayName = "BFT Device";
        descriptionUse = "<t color='#9cf953'>Use: </t>To display friendly units on map";
        descriptionShort = "Enables accurate positioning for own and friendly units";
        picture = "\redd_bft\data\frame_inv_pre.paa";
		model = "\redd_bft\frame.p3d";
        
        class ItemInfo {
            mass = 8;
        };
    };
};

class CfgVehicles {

    class Item_ItemGPS;

    class redd_bft_groundItem: Item_ItemGPS {
        author = "redd";
        displayName = "BFT Device";
        vehicleClass = "Items";
        model = "\redd_bft\frame_h.p3d";
        
        class TransportItems {

            class _xx_redd_bft_inventoryItem {  
                name = "redd_bft_inventoryItem";
                count = 1;
            };
        };
    };

    class Man;

    class CAManBase: Man {

        class ACE_SelfActions {
            
            class Redd_BFT_Support_ACE_action {
                displayName = "Support Status";
                showDisabled = 0;
                condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                runOnHover = 1;
                exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                icon = "\a3\ui_f\data\igui\cfg\simpletasks\types\interact_ca.paa";

                class Redd_BFT_Support_ACE_action_1 {
                    displayName = "Status #1: Ready in Base";
                    condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                    exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                    icon = "\a3\ui_f\data\igui\cfg\actions\ico_on_ca.paa";
                    statement = "(leader group _player) setVariable ['ReddBFTstatus', '1', true]";
                };

                class Redd_BFT_Support_ACE_action_2 {
                    displayName = "Status #2: Ready on radio";
                    condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                    exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                    icon = "\a3\ui_f\data\igui\cfg\actions\ico_on_ca.paa";
                    statement = "(leader group _player) setVariable ['ReddBFTstatus', '2', true]";
                };

                class Redd_BFT_Support_ACE_action_3 {
                    displayName = "Status #3: Preparing assignment";
                    condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                    exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                    icon = "\a3\ui_f\data\igui\cfg\actions\ico_on_ca.paa";
                    statement = "(leader group _player) setVariable ['ReddBFTstatus', '3', true]";
                };

                class Redd_BFT_Support_ACE_action_4 {
                    displayName = "Status #4: Moving to supply point";
                    condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                    exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                    icon = "\a3\ui_f\data\igui\cfg\actions\ico_on_ca.paa";
                    statement = "(leader group _player) setVariable ['ReddBFTstatus', '4', true]";
                };

                class Redd_BFT_Support_ACE_action_5 {
                    displayName = "Status #5: Reached supply point";
                    condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                    exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                    icon = "\a3\ui_f\data\igui\cfg\actions\ico_on_ca.paa";
                    statement = "(leader group _player) setVariable ['ReddBFTstatus', '5', true]";
                };

                class Redd_BFT_Support_ACE_action_6 {
                    displayName = "Status #6: Leaving supply point";
                    condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                    exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                    icon = "\a3\ui_f\data\igui\cfg\actions\ico_on_ca.paa";
                    statement = "(leader group _player) setVariable ['ReddBFTstatus', '6', true]";
                };

                class Redd_BFT_Support_ACE_action_7 {
                    displayName = "Status #7: Not ready";
                    condition = "((leader group _player) getVariable ['IsReddBFTsupport', false])";
                    exceptions[] = {"isNotInside", "isNotSitting", "notOnMap"};
                    icon = "\a3\ui_f\data\igui\cfg\actions\ico_on_ca.paa";
                    statement = "(leader group _player) setVariable ['ReddBFTstatus', '7', true]";
                };
            };
        };

        class Attributes {

            class Set_ReddBftGroupName {
                displayName = "Set BFT group name";
                tooltip = "Sets BFT group name for every unit in group, only leader needs this attribute. Group name must be unique";
                property = "Set_ReddBftGroupName";
                control = "Edit";
                expression = "_this setVariable ['ReddBFTGroupName', _value, true];";
                defaultValue = "''";
                typeName = "STRING";
                unique = 1;
            };

            class Set_ReddBftUnitType {
                displayName = "Choose BFT marker type";
                tooltip = "Sets BFT marker for every unit in group, only leader needs this attribute";
                property = "Set_ReddBftUnitType";
                control = "Combo";
                expression = "_this setVariable ['ReddBftUnitType', _value, true];";
                defaultValue = "inf";
                typeName = "STRING";

                class values {

                    class Inf {
                        name = "Infantry";
                        value = "inf";
                    };

                    class MotInf {
                        name = "Motorized Infantry";
                        value = "motor_inf";
                    };

                    class MechInf {
                        name = "Mechanized Infantry";
                        value = "mech_inf";
                    };

                    class Armor {
                        name = "Armor";
                        value = "armor";
                    };

                    class Recon {
                        name = "Recon";
                        value = "recon";
                    };

                    class Helicopter {
                        name = "Helicopter";
                        value = "air";
                    };

                    class Plane {
                        name = "Plane";
                        value = "plane";
                    };

                    class UAV {
                        name = "UAV";
                        value = "uav";
                    };

                    class Naval {
                        name = "Naval";
                        value = "naval";
                    };

                    class Medical {
                        name = "Medical";
                        value = "med";
                    };

                    class Artillery {
                        name = "Artillery";
                        value = "art";
                    };

                    class HQ {
                        name = "HQ";
                        value = "hq";
                    };

                    class Support {
                        name = "Support";
                        value = "support";
                    };

                    class Maintenance {
                        name = "Maintenance";
                        value = "maint";
                    };
                };
            };

            class Set_SupportUnit {
                displayName = "Is Support Unit";
                tooltip = "Gives player BFT support status ACE actions, only leader needs this attribute";
                property = "Set_SupportUnit";
                control = "Checkbox";
                expression = "_this setVariable ['IsReddBFTsupport', _value, true]; _this setVariable ['ReddBFTstatus', '-', true];";
                defaultValue = "false";
            };
        };
    };
};

class Extended_PostInit_EventHandlers {

    class Redd_Bft_post_init_event {
        clientInit = "call compile preprocessFileLineNumbers '\redd_bft\XEH_postInitClient.sqf'";
    };
};

class Extended_PreInit_EventHandlers {

    class Redd_Bft_pre_init_event {
        init = "call compile preprocessFileLineNumbers '\redd_bft\XEH_preInit.sqf'";
        serverInit = "call compile preprocessFileLineNumbers '\redd_bft\XEH_preInitServer.sqf'";
    };
};

class CfgFunctions {

    class redd_bft {
        tag = "redd";
        
        class init {
            file="\redd_bft\functions";
            class BFTdisplay {};
            class drawInternalMarker {};
            class drawMarker {};
            class getBFTdata {};
            class getBftItem {};
            class getStatus {};
            class updateBFT {};
            class updateSupport {};
            class toggleLeftRightDisplay {};
            class toggleLeftRightSupport {};
        };
    };
};